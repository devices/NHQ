import time
import serial


# SERIAL = "/dev/ttyS0"
SERIAL = "/dev/ttyUSB0"

"""
USAGE:
import iseg_power_supply
with iseg_power_supply.iseg_power_supply() as ips:
    print "status=%r" % ips.get_status()
    print "status=", ips.get_status_desc()
    print "module_status=%r" % ips.get_module_status()
    print "module_status=",
    print ips.get_module_status_desc()
    print "ID=%s" %  ", ".join([str(x) for x in ips.get_module_id()])
    print "voltage=%g V" % ips.get_voltage_measured()
    print "sp_volt=%g V" % ips.get_voltage_setpoint()
    print "current=%g A" % ips.get_current_measured()
    print "ramp_speed=%g V/s" % ips.get_voltage_ramp_speed()
    ips.set_voltage_ramp_speed(3)
    ips.set_voltage_setpoint(23.45)
    print "status=", ips.get_status_desc()
    print "sp_volt=%g V" % ips.get_voltage_setpoint()
    print "voltage=%g V" % ips.get_voltage_measured()
    print "voltage limit=%g V" % ips.get_voltage_limit()
    print "current limit=%g A" % ips.get_current_limit()

or:

import iseg_power_supply
ips = iseg_power_supply.iseg_power_supply()
ips.test_all()
ips.test_loop_ramp()

"""

NHQ_STATUS = {
    "ON" : "Output voltage according to set voltage",
    "OFF": "Channel front panel switch is off",
    "MAN": "Channel is on, set to manual mode",
    "ERR": "Vmax or Imax is or was exceeded",
    "INH": "inhibit signal is or was active",
    "QUA": "Quality of output voltage not given at present",
    "L2H": "Output voltage increasing",
    "H2L": "Output voltage decreasing",
    "LAS": "Look at status (after G-command)",
    "TRP": "Current trip was active"
}

NHQ_ERROR_CODES = {
    "????" : "Syntax error",
    "?WCN" : "Wrong channel number (missed bit in communication ?)",
    "?TOT" : "Timeout error",
    "? UMAX" : "Voltage setpoint exceeds voltage limit"
}

NHQ_MODULE_STATUS = {
    "QUA": "Quality of output voltage is not guaranteed",
    "ERR": "Vmax or Imax exceeded",
    "INH": "External INHIBIT signal is active",
    "KILL_ENA": "KILL-ENABLE is active",
    "OFF": "Front panel HV-ON switch is OFF",
    "POL": "Polarity is set to POSITIVE",
    "MAN": "Control is manual"
}

# Auto start features are not implemented.
# Current trip features are not implemented.

class iseg_power_supply():
    def __enter__(self):
        print "__enter__"
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        print "__exit__ : closing serial line"
        self.sl.close()

    def __init__(self):
        print "__init__"
        print "open serial line"
        self.sl = serial.Serial(SERIAL, 9600, bytesize=8, parity='N', stopbits=1, timeout=0.0)

        self.old_voltage_measured = 0
        self.old_voltage_setpoint = 0
        self.old_voltage_limit = 0
        self.old_current_measured = 0
        self.old_current_limit = 0
        self.old_ramp_speed = 0
        self.old_status = ""
        self.old_module_status = ""

        self.Imax = None
        self.Vmax = None

        self.sl.flush()
        time.sleep(0.1)

        (serial_number, version, V_out_max, I_out_max) = self.get_module_id()
        print serial_number, version, V_out_max, I_out_max
        
    def iseg_sleep_char(self):
        time.sleep(0.01)
    
    def iseg_sleep_ans(self):
        # 0.3 is really not enough
        # 0.5 is not enough !%$#!$!@#
        time.sleep(0.3)

    def iseg_write(self, cmd):
        self.sl.write(cmd  + "\r\n")
        self.iseg_sleep_ans()
        _ans = self.sl.readline()
        _ans = _ans.strip()

        # skip "\r\n"
        if cmd.rfind("D1") or cmd.rfind("V1") or cmd.rfind("W") or cmd.rfind("L1") or cmd.rfind("LB1") or cmd.rfind("LS1") :
            self.iseg_sleep_ans()
            _ = self.sl.readline()

        if cmd != _ans:
            print "Communication error : "
            print " SEND : %r" % cmd
            print " ECHO : %r" % _ans
        
    def iseg_write_read(self, command):
        """
        <command> : string of command (ex: U1 I1 D1 V1 S1 T1)
        """
        self.sl.write("%s\r\n" % command)
        self.iseg_sleep_ans()
        _ans = self.sl.readline()
        if _ans.strip() != command:
            print "petit pb de %s...(read=%r) on attend un peu..." % (command, _ans)
            time.sleep(0.1)
            _ans = self.sl.readline()
            print "new read=%r" % _ans
            
        self.iseg_sleep_ans()
        _value = self.sl.readline().strip()
    
        return _value
        
    def get_voltage_measured(self):
        _ans = self.iseg_write_read("U1")
        # _ans looks like "+00001-01"
    
        try:
            # calculate voltage from mantissa and exp...
            mantissa =  float(_ans[0:6])
            signed_exposant = float(_ans[6:9])
            voltage = mantissa* pow(10, signed_exposant)
            self.old_voltage_measured = voltage
        except:
            print("Error in reading voltage measured (got %r)" % _ans)
            print( "  -> Use last stored value")
            voltage = self.old_voltage_measured
                
        return voltage

    def set_voltage_setpoint(self, vsp_value):
        # Writes voltage setpoint.
        self.iseg_write("D1=%07.2f" % vsp_value)
        self.iseg_sleep_ans()
        
        # Starts voltage ramp.
        self.iseg_write("G1")
        self.iseg_sleep_ans()
        
    def get_voltage_setpoint(self):
        _ans = self.iseg_write_read("D1")
        # _ans looks like "00001-01"
        try:
            # calculate voltage from mantissa and exp...
            mantissa =  float(_ans[0:5])
            signed_exposant = float(_ans[5:8])
            voltage_sp = mantissa* pow(10, signed_exposant)
            self.old_voltage_setpoint = voltage_sp
        except:
            print "Error in reading voltage setpoint"
            print "  -> Use last stored value"
            voltage_sp = self.old_voltage_setpoint
        
        return voltage_sp

    def set_voltage_ramp_speed(self, speed):
        """
        <speed> : integer in Volts/second
        """
        self.iseg_write("V1=%03d" % int(speed))
        self.iseg_sleep_ans()

    def get_voltage_ramp_speed(self):
        """
        Returns int value in Volts/seconds.
        """
        _ans = self.iseg_write_read("V1")
        # _ans looks like : "002"
        try:
            ramp_speed = float(_ans)
            self.old_ramp_speed = ramp_speed
        except:
            print "Error in reading ramp speed"
            print "  -> Use last stored value"
            ramp_speed = self.old_ramp_speed
        return ramp_speed

    def get_voltage_limit(self):
        """
        Returns int value in Volts
        """
        _ans = self.iseg_write_read("M1")
        # _ans looks like : "012"  in % of VoutMax
        try:
            voltage_limit = float(_ans)
            if self.Vmax is not None:
                voltage_limit = voltage_limit * self.Vmax / 100
            self.old_voltage_limit = voltage_limit
        except:
            print "Error in reading voltage limit"
            print "  -> Use last stored value"
            voltage_limit = self.voltage_limit
        return voltage_limit

    def get_current_limit(self):
        """
        Returns int value in Ampers.
        """
        _ans = self.iseg_write_read("N1")
        # _ans looks like : "012"  in % of IoutMax
        try:
            current_limit = float(_ans)
            if self.Imax is not None:
                current_limit = current_limit * self.Imax / 100
            self.old_current_limit = current_limit
        except:
            print "Error in reading current limit"
            print "  -> Use last stored value"
            current_limit = self.current_limit
        return current_limit
   
    def get_current_measured(self):
        _ans = self.iseg_write_read("I1")
        # _ans looks like "00000-09"
    
        try:
            # calculate voltage from mantissa and exp...
            mantissa =  float(_ans[0:5])
            signed_exposant = float(_ans[5:8])
            current = mantissa* pow(10, signed_exposant)
            self.old_current_measured = current
        except:
            print "Error in reading current measured"
            print "  -> Use last stored value"
            current = self.old_current_measured
          
        return current
    
    def set_voltage(self, voltage_setpoint):
        """
        <voltage_setpoint> in Volts
        """
        voltage_setpoint
    
        
    def get_status(self):
        _ans = self.iseg_write_read("S1")
        # _ans looks like : "S1=XX"  where XX is one of NHQ_STATUS
        print "status=", _ans 
        try:
            status = _ans
            self.old_status = status
        except:
            print "Error in reading status"
            print "  -> Use last stored value"
            status = self.old_status

        return status

    def get_status_desc(self):
        status = self.get_status().split("=")[1]
        status_string = NHQ_STATUS[status]

        return status_string
    
    def get_module_status(self):
        _ans = self.iseg_write_read("T1")
        # _ans lookd like '004'
        # print _ans
        _ans = int(_ans)
        try:
            module_status = ""
            # if _ans & 0x01: module_status += 
            if _ans & 0x02:
                module_status += "MAN "
            if _ans & 0x04:
                module_status += "POL "
            if _ans & 0x08:
                module_status += "OFF "
            if _ans & 0x10:
                module_status += "KILL_ENA "
            if _ans & 0x20:
                module_status += "INH "
            if _ans & 0x40:
                module_status += "ERR "
            if _ans & 0x80:
                module_status += "QUA "
            
            self.old_module_status = module_status
        except:
            print "Error in reading module_status"
            print "  -> Use last stored value"
            module_status = self.old_module_status

        return module_status

    def get_module_status_desc(self):
        status_list = self.get_module_status().split()
        status_strings = list()
        
        for status in status_list:
            status_strings.append(NHQ_MODULE_STATUS[status])

        return status_strings
    
    def get_module_id(self):
        self.sl.write("#")
        self.iseg_sleep_char()
        self.sl.write("\r")
        self.iseg_sleep_char()
        self.sl.write("\n")
        
        # Reads echo from controller.
        self.iseg_sleep_char()
        _ans = self.sl.readline()
    
        # Reads and parses answer.
        self.iseg_sleep_ans()
        _ans =  self.sl.readline()
        # _ans looks like : "484442;3.14;2000V;6mA"
        serial_number, version, V_out_max, I_out_max = _ans.strip().split(";")

        self.Vmax = int(V_out_max.rstrip("V"))
        self.Imax = int(I_out_max.rstrip("mA"))

        return (serial_number, version, V_out_max, I_out_max)


    def test_all(self):
        print "status=%r" % self.get_status()
        print "status=", self.get_status_desc()
        print "module_status=%r" % self.get_module_status()
        print "module_status="
        print self.get_module_status_desc()
        print "ID=%s" %  ", ".join([str(x) for x in self.get_module_id()])
        print "voltage=%g V" % self.get_voltage_measured()
        print "sp_volt=%g V" % self.get_voltage_setpoint()
        print "current=%g A" % self.get_current_measured()
        print "ramp_speed=%g V/s" % self.get_voltage_ramp_speed()
        self.set_voltage_ramp_speed(3)
        self.set_voltage_setpoint(23.45)
        print "status=", self.get_status_desc()
        print "sp_volt=%g V" % self.get_voltage_setpoint()
        print "voltage=%g V" % self.get_voltage_measured()
        print ("voltage limit=%g V" % self.get_voltage_limit())
        print ("current limit=%g A" % self.get_current_limit())
        

    def test_loop_ramp(self):
        vsp = 0
        ii = 0
        self.set_voltage_ramp_speed(10)
        while(True):
            if vsp > 20:
                vsp = 0
            self.set_voltage_setpoint(vsp)
            time.sleep(0.1)
            vread = self.get_voltage_measured()
            print("%05d : set %gV ,  read: %gV" % (ii, vsp, vread))
            
            vsp += 1
            ii += 1

            

        
#  'applySettingsDict',
#  'baudrate',
#  'bytesize',
#  'close',
#  'closed',
#  'drainOutput',
#  'dsrdtr',
#  'fileno',
#  'flowControl',
#  'flush',
#  'flushInput',
#  'flushOutput',
#  'getBaudrate',
#  'getByteSize',
#  'getCD',
#  'getCTS',
#  'getDSR',
#  'getDsrDtr',
#  'getInterCharTimeout',
#  'getParity',
#  'getPort',
#  'getRI',
#  'getRtsCts',
#  'getSettingsDict',
#  'getStopbits',
#  'getSupportedBaudrates',
#  'getSupportedByteSizes',
#  'getSupportedParities',
#  'getSupportedStopbits',
#  'getTimeout',
#  'getWriteTimeout',
#  'getXonXoff',
#  'inWaiting',
#  'interCharTimeout',
#  'isOpen',
#  'isatty',
#  'makeDeviceName',
#  'next',
#  'nonblocking',
#  'open',
#  'parity',
#  'port',
#  'read',
#  'readable',
#  'readall',
#  'readinto',
#  'readline',
#  'readlines',
#  'rtscts',
#  'seek',
#  'seekable',
#  'sendBreak',
#  'setBaudrate',
#  'setBreak',
#  'setByteSize',
#  'setDTR',
#  'setDsrDtr',
#  'setInterCharTimeout',
#  'setParity',
#  'setPort',
#  'setRTS',
#  'setRtsCts',
#  'setStopbits',
#  'setTimeout',
#  'setWriteTimeout',
#  'setXonXoff',
#  'stopbits',
#  'tell',
#  'timeout',
#  'truncate',
#  'writable',
#  'write',
#  'writeTimeout',
#  'writelines',
#  'xonxoff']

# !!!!!!! 913    +00001-01   00000-09
# !!!!!!! 914    +00001-01   00000-09
# !!!!!!! 915    +00001-01   00000-09
# !!!!!!! 916    +00001-01   00000-09
# !!!!!!! 917       petit pb de current... on attend un peu...
# !!!!!!! +00000-01
# !!!!!!! 918    petit pb de voltage... on attend un peu...
# !!!!!!! ?WCN   00000-09                <-------------------------WTF WCN ??? never sent that !!!
# !!!!!!! 919    +00000-01   00000-09
# !!!!!!! 920    +00001-01   00000-09




###   march 2018 : 

# 00245 : set 14V ,  read: 14.3V
# 00246 : set 15V ,  read: 15.3V
# petit pb de U1...(read='S1=L2H\r\n') on attend un peu...
# new read='U1\r\n'
# 00247 : set 16V ,  read: 15.3V
# 00248 : set 17V ,  read: 17.3V
# 
# 
# 
# 
# 00766 : set 10V ,  read: 10.3V
# 00767 : set 11V ,  read: 11.3V
# Communication error : 
#  SEND : 'G1'
#  ECHO : ''
# petit pb de U1...(read='S1=L2H\r\n') on attend un peu...
# new read='U1\r\n'
# 00768 : set 12V ,  read: 12.3V
# 00769 : set 13V ,  read: 13.3V
